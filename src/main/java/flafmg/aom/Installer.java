package flafmg.aom;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.util.*;
import java.util.List;
import java.util.jar.*;
import java.util.stream.*;
import java.util.zip.*;

//bad spagethi code :D, i hate it!
// - flafmg, 2024
//but it works for what it needs to

public class Installer {
    private final String gameExecutablePath;
    private final InstallerLogGUI logGUI;
    private final Thread installThread;

    public Installer(String gameExecutablePath) {
        this.gameExecutablePath = gameExecutablePath;
        logGUI = new InstallerLogGUI();
        installThread = new Thread(this::install);
    }

    public void start() {
        installThread.start();
    }

    private void install() {
        if (!isValidAoC2Exe()) {
            showError("The selected file is not valid. Please make sure to select the AoC2.exe file.");
            return;
        }
        logGUI.updateProgress(10);
        if (!createTempFolder()) {
            showError("Failed to create temporary folder.");
            return;
        }
        logGUI.updateProgress(20);
        if (!extractResources()) {
            showError("Failed to extract resources.");
            return;
        }
        logGUI.updateProgress(30);
        if (!extractAoC2Jar()) {
            showError("Failed to extract AoC2.jar.");
            return;
        }
        logGUI.updateProgress(40);
        if (!decompileProject()) {
            showError("Failed to decompile project.");
            return;
        }
        logGUI.updateProgress(50);
        if (!modifyJavaFiles()) {
            showError("Failed to make aoc2 base classes public access.");
            return;
        }
        logGUI.updateProgress(60);
        if (!applyPatches()) {
            showError("Failed to apply patches.");
            return;
        }
        logGUI.updateProgress(70);
        if (!recompileClasses()) {
            showError("Failed to recompile classes.");
            return;
        }
        logGUI.updateProgress(80);
        if (!createAoMJar() || !createExecutable()) {
            showError("Failed to create executable");
            return;
        }
        logGUI.updateProgress(100);
        JOptionPane.showMessageDialog(null, "Installation completed successfully!");
        logGUI.dispose();
        openGameExecutableFolder();
        finish();
    }
    private void openGameExecutableFolder() {
        File gameExeFile = new File(gameExecutablePath);
        File parentFolder = gameExeFile.getParentFile();
        if (parentFolder != null && parentFolder.exists()) {
            try {
                Desktop.getDesktop().open(parentFolder);
                log("Opened game directory: " + parentFolder.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
                log("Failed to open game directory: " + parentFolder.getAbsolutePath());
            }
        } else {
            log("Game directory not found: " + (parentFolder != null ? parentFolder.getAbsolutePath() : "null"));
        }
    }
    private void log(String message) {
        System.out.println(message);
        logGUI.appendLog(message);
    }

    private void showError(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
        logGUI.dispose();
        finish();
    }

    private boolean createTempFolder() {
        Path tempFolderPath = Paths.get(".temp/decomp");
        try {
            Files.createDirectories(tempFolderPath);
            log(".temp/decomp folder created.");
            return true;
        } catch (IOException e) {
            log("Failed to create .temp/decomp folder.");
            e.printStackTrace();
            return false;
        }
    }
    private boolean extractResources(){
        log("Extracting resources...");
        boolean extracted =
                extractResource("vineflower.jar", ".temp/vineflower.jar") &&
                extractResource("aom.zip", ".temp/decomp") &&
                extractResource("patches.zip", ".temp/patches") &&
                extractResource("launch4j.zip", ".temp/launch4j") &&
                extractResource("l4jconfig.xml", ".temp/aomexeconfig.xml") &&
                extractResource("aom_48x48.ico", ".temp/aom_48x48.ico");
        if (extracted) {
            if (System.getProperty("os.name").toLowerCase().contains("linux")) {
                try {
                    setExecutable(".temp/launch4j/bin/windres");
                    setExecutable(".temp/launch4j/bin/ld");
                } catch (IOException e) {
                    e.printStackTrace();
                    log("Failed to set executable permissions on Linux.");
                    return false;
                }
            }
        }
        return extracted;
    }

    private void setExecutable(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        Set<PosixFilePermission> perms = EnumSet.of(
                PosixFilePermission.OWNER_READ, PosixFilePermission.OWNER_WRITE, PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.GROUP_READ, PosixFilePermission.GROUP_EXECUTE,
                PosixFilePermission.OTHERS_READ, PosixFilePermission.OTHERS_EXECUTE
        );
        Files.setPosixFilePermissions(path, perms);
        log("Set executable permissions: " + filePath);
    }


    private boolean extractResource(String resourceName, String targetDir) {
        try {
            File targetFile = extractResourceToTemp(resourceName, resourceName);
            if (resourceName.endsWith(".zip")) {
                unzipResource(targetFile, targetDir);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private File extractResourceToTemp(String resourceName, String targetFileName) throws IOException {
        InputStream resourceStream = Installer.class.getResourceAsStream("/" + resourceName);
        if (resourceStream == null) {
            throw new IOException(resourceName + " not found in resources.");
        }
        File targetFile = new File(".temp", targetFileName);
        Files.copy(resourceStream, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        return targetFile;
    }

    private void unzipResource(File zipFile, String targetDir) throws IOException {
        try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFile))) {
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                if (!entry.isDirectory()) {
                    File file = new File(targetDir, entry.getName());
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                    }
                    Files.copy(zipInputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
            }
        }
    }

    private boolean extractAoC2Jar() {
        try {
            File jarFile = new File(gameExecutablePath);
            File decompFolder = new File(".temp/decomp");
            if (!decompFolder.exists()) {
                decompFolder.mkdirs();
            }
            try (JarFile jar = new JarFile(jarFile)) {
                Enumeration<JarEntry> entries = jar.entries();
                while (entries.hasMoreElements()) {
                    JarEntry entry = entries.nextElement();
                    if (!entry.getName().startsWith("age/") && !entry.isDirectory()) {
                        File entryFile = new File(decompFolder, entry.getName());
                        if (!entryFile.getParentFile().exists()) {
                            entryFile.getParentFile().mkdirs();
                        }
                        try (InputStream is = jar.getInputStream(entry);
                             FileOutputStream fos = new FileOutputStream(entryFile)) {
                            byte[] buffer = new byte[1024];
                            int bytesRead;
                            while ((bytesRead = is.read(buffer)) != -1) {
                                fos.write(buffer, 0, bytesRead);
                            }
                        }
                    }
                }
            }
            log("AoC2.jar extracted to .temp/decomp without 'age' folder.");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isValidAoC2Exe() {
        File exeFile = new File(gameExecutablePath);
        if (!exeFile.getName().equalsIgnoreCase("AoC2.exe")) {
            log("The selected file is not AoC2.exe");
            return false;
        }
        try (ZipFile zipFile = new ZipFile(exeFile)) {
            return zipFile.stream().anyMatch(entry -> entry.getName().startsWith("age/of/civilizations2"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        log("The AoC2.exe does not contain the package age.of.civilizations2");
        return false;
    }

    private boolean decompileProject() {
        log("Decompiling game (this can take a while)");
        try {
            File vineflowerJar = new File(".temp/vineflower.jar");
            File tempExeCopy = new File(".temp/AoC2.jar");
            Files.copy(new File(gameExecutablePath).toPath(), tempExeCopy.toPath(), StandardCopyOption.REPLACE_EXISTING);
            String[] command = {
                    "java", "-Xmx1500m", "-jar", vineflowerJar.getAbsolutePath(),
                    "--thread-count=4",
                    "--skip-extra-files=1",
                    "--only=age",
                    tempExeCopy.getAbsolutePath(),
                    ".temp/decomp"
            };
            if (!executeCommand(command)) {
                return false;
            }
            log("Decompilation process completed.");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean modifyJavaFiles() {
        Path ageDir = Paths.get(".temp/decomp/age/of/civilizations2");
        try (Stream<Path> paths = Files.walk(ageDir)) {
            paths.filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".java"))
                    .forEach(this::modifyJavaFile);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void modifyJavaFile(Path javaFilePath) {
        try {
            String content = new String(Files.readAllBytes(javaFilePath), StandardCharsets.UTF_8);
            content = content.replaceAll("\\bprotected\\b", "public");
            content = content.replaceAll("(?<!public\\s|private\\s|static\\s|abstract\\s|\\.)\\bclass\\b", "public class");
            Files.write(javaFilePath, content.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean applyPatches() {
        Path patchesDir = Paths.get(".temp/patches");
        try (Stream<Path> paths = Files.walk(patchesDir)) {
            paths.filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".patch"))
                    .forEach(this::applyPatch);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void applyPatch(Path patchFile) {
        String patchPath = patchFile.toAbsolutePath().toString();
        Path targetDir = Paths.get(".temp/decomp");
        String targetDirString = targetDir.toString();
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            targetDirString = targetDirString.replace("\\", "/");
        }
        String[] command = {"git", "apply", "--unidiff-zero", "--directory=" + targetDirString, patchPath};
        if(executeCommand(command)) {
            log("Patch " + patchFile.toFile().getName() + " applied");
        }
    }


    private boolean recompileClasses() {
        log("recompiling game (this can take a while)");
        Path decompPath = Paths.get(".temp/decomp");
        int batchSize = 250; // since windows for some reason cant handle too many commands
        try (Stream<Path> paths = Files.walk(decompPath)) {
            List<Path> javaFiles = paths.filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".java"))
                    .collect(Collectors.toList());

            String classpath = decompPath.toString();
            List<String> commandBase = Arrays.asList("javac", "-encoding", "ISO-8859-1", "--release", "8", "-Xlint:-options", "-cp", classpath, "-d", decompPath.toString());

            for (int i = 0; i < javaFiles.size(); i += batchSize) {
                List<String> command = new ArrayList<>(commandBase);
                javaFiles.subList(i, Math.min(i + batchSize, javaFiles.size()))
                        .forEach(file -> command.add(file.toString()));
                log("compiling " + i + " / " + javaFiles.size());
                if (!executeCommand(command.toArray(new String[0]))) {
                    return false;
                }
            }

            for (Path javaFile : javaFiles) {
                Files.delete(javaFile);
            }

            log("Recompilation process completed.");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


    private boolean createAoMJar() {
        try {
            File aoMJarFile = new File(".temp/AoM.jar");
            File gameFolderJar = new File(gameExecutablePath.replace("AoC2.exe", "AoM.jar"));
            try (JarOutputStream jarOut = new JarOutputStream(new FileOutputStream(aoMJarFile))) {
                Path decompPath = Paths.get(".temp/decomp");
                try (Stream<Path> paths = Files.walk(decompPath)) {
                    paths.filter(Files::isRegularFile)
                            .forEach(path -> {
                                String entryName = decompPath.relativize(path).toString().replace("\\", "/");
                                try {
                                    jarOut.putNextEntry(new JarEntry(entryName));
                                    Files.copy(path, jarOut);
                                    jarOut.closeEntry();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                }
            }
            Files.copy(aoMJarFile.toPath(), gameFolderJar.toPath(), StandardCopyOption.REPLACE_EXISTING);
            log("jar created successfully.");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean createExecutable() {
        try {
            File launch4jDir = new File(".temp/launch4j");
            File configXml = new File(".temp/l4jconfig.xml");
            String[] command = {
                    "java", "-jar", new File(launch4jDir, "launch4j.jar").getAbsolutePath(), configXml.getAbsolutePath()
            };
            if (!executeCommand(command)) {
                return false;
            }

            File exeFile = new File(".temp/AoM.exe");
            File gameFolderExe = new File(gameExecutablePath.replace("AoC2.exe", "AoM.exe"));
            Files.move(exeFile.toPath(), gameFolderExe.toPath(), StandardCopyOption.REPLACE_EXISTING);

            log("Executable created successfully.");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean executeCommand(String[] command) {
        try {
            Process process = new ProcessBuilder(command).redirectErrorStream(true).start();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    log(line);
                }
            }
            int exitCode = process.waitFor();
            return exitCode == 0;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void finish() {
        Path tempFolderPath = Paths.get(".temp");
        try {
            if (Files.exists(tempFolderPath)) {
                Files.walk(tempFolderPath)
                     .sorted(Comparator.reverseOrder())
                     .map(Path::toFile)
                     .forEach(File::delete);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        installThread.interrupt();
    }
}
