package flafmg.aom;

import javax.swing.*;
import java.awt.*;

public class InstallerLogGUI extends JFrame {

    private JProgressBar progressBar;
    private JTextArea logTextArea;
    private JScrollPane scrollPane;

    public InstallerLogGUI() {
        setTitle("Installer Log");
        setSize(500, 400);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        progressBar = new JProgressBar();
        progressBar.setStringPainted(true);
        add(progressBar, BorderLayout.NORTH);

        logTextArea = new JTextArea();
        logTextArea.setEditable(false);
        scrollPane = new JScrollPane(logTextArea);
        add(scrollPane, BorderLayout.CENTER);

        setVisible(true);
    }

    public void updateProgress(int progress) {
        progressBar.setValue(progress);
    }

    public void appendLog(String log) {
        JScrollBar verticalScrollBar = scrollPane.getVerticalScrollBar();

        logTextArea.append(log + "\n");
        logTextArea.setCaretPosition(logTextArea.getDocument().getLength());
    }
}
