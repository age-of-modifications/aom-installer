package flafmg.aom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

public class AoMInstaller {
    private JTextField gamePathTextField;
    private Installer installer;

    public AoMInstaller() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        JFrame frame = new JFrame("AoM Installer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(318, 293);
        frame.setResizable(false);
        frame.setLayout(new BorderLayout());
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                handleClose();
            }
        });


        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(null);

        ImageIcon icon = new ImageIcon(new ImageIcon(getClass().getResource("/aom.png")).getImage().getScaledInstance(318, 138, Image.SCALE_SMOOTH));
        JLabel logoLabel = new JLabel(icon);
        logoLabel.setBounds(0, 0, 318, 138);
        mainPanel.add(logoLabel);

        JLabel welcomeLabel = new JLabel("Welcome to AoM installer!");
        welcomeLabel.setBounds(73, 130, 200, 15);
        mainPanel.add(welcomeLabel);

        JLabel instructionLabel = new JLabel("This will install AoM (alpha 1) to your game");
        instructionLabel.setBounds(18, 146, 300, 15);
        mainPanel.add(instructionLabel);

        gamePathTextField = new JTextField();
        gamePathTextField.setBounds(14, 176, 240,24);
        gamePathTextField.setToolTipText("Game exe path");
        gamePathTextField.setEditable(false);
        mainPanel.add(gamePathTextField);

        JButton browseButton = new JButton("...");
        browseButton.setBounds(256, 176, 30, 24);
        browseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                handleBrowseButtonAction();
            }
        });
        mainPanel.add(browseButton);

        JLabel discordLink = new JLabel("<HTML><FONT color=\"#000099\"><U>discord</U></FONT></HTML>");
        discordLink.setBounds(14, 200, 50, 20);
        discordLink.setCursor(new Cursor(Cursor.HAND_CURSOR));
        discordLink.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    handleDiscordLinkAction();
                } catch (URISyntaxException | IOException uriSyntaxException) {
                    uriSyntaxException.printStackTrace();
                }
            }
        });
        mainPanel.add(discordLink);

        JLabel separatorLabel = new JLabel("|");
        separatorLabel.setBounds(70, 200, 10, 20);
        mainPanel.add(separatorLabel);

        JLabel gitlabLink = new JLabel("<HTML><FONT color=\"#000099\"><U>gitlab</U></FONT></HTML>");
        gitlabLink.setBounds(80, 200, 50, 20);
        gitlabLink.setCursor(new Cursor(Cursor.HAND_CURSOR));
        gitlabLink.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    handleGitlabLinkAction();
                } catch (URISyntaxException | IOException uriSyntaxException) {
                    uriSyntaxException.printStackTrace();
                }
            }
        });
        mainPanel.add(gitlabLink);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.setBounds(130, 230, 80, 24);
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                handleCancelButtonAction();
            }
        });
        mainPanel.add(cancelButton);

        JButton okButton = new JButton("Ok");
        okButton.setBounds(215, 230, 80, 24);
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    handleOkButtonAction();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        mainPanel.add(okButton);

        JSeparator separator = new JSeparator();
        separator.setBounds(0, 222, 318, 16);
        mainPanel.add(separator);

        frame.add(mainPanel, BorderLayout.CENTER);
        frame.setVisible(true);

        autoDetectGamePath();
    }

    private void handleBrowseButtonAction() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(new javax.swing.filechooser.FileNameExtensionFilter("Executable Files", "exe"));
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            gamePathTextField.setText(selectedFile.getAbsolutePath());
        }
    }

    private void handleDiscordLinkAction() throws URISyntaxException, IOException {
        Desktop desktop = Desktop.getDesktop();
        desktop.browse(new URI("https://discord.gg/5gX297vNDC"));
    }

    private void handleGitlabLinkAction() throws URISyntaxException, IOException {
        Desktop desktop = Desktop.getDesktop();
        desktop.browse(new URI("https://gitlab.com/age-of-modifications/age-of-modifications"));
    }

    private void handleCancelButtonAction() {
        handleClose();
    }

    private void handleOkButtonAction() throws URISyntaxException, IOException {
        Desktop desktop = Desktop.getDesktop();

        if (!isProgramInstalled("git", "--version")) {
            JOptionPane.showMessageDialog(null, "Git is not installed. Please install Git and try again.", "Error", JOptionPane.ERROR_MESSAGE);
            desktop.browse(new URI("https://git-scm.com/"));
            return;
        }

        if (!isProgramInstalled("javac", "-version")) {
            JOptionPane.showMessageDialog(null, "JDK is not installed or not found in PATH. Please install JDK 1.8 or higher and try again.", "Error", JOptionPane.ERROR_MESSAGE);
            desktop.browse(new URI("https://www.oracle.com/br/java/technologies/downloads/#java17"));
            return;
        }

        String gamePath = gamePathTextField.getText();
        if (installer != null) {
            installer.finish();
            installer = null;
        }
        installer = new Installer(gamePath);
        installer.start();
    }
    private void handleClose() {
        if(installer != null){
            installer.finish();
        }
        System.exit(0);
    }

    private String findGamePath() {
        String gamePath = null;
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("linux")) {
            String steamFolderPath = System.getProperty("user.home") + "/.local/share/Steam/steamapps/common/AoCII";
            File steamFolder = new File(steamFolderPath);
            if (steamFolder.exists()) {
                File[] files = steamFolder.listFiles((dir, name) -> name.endsWith(".exe") && name.equalsIgnoreCase("AoC2.exe"));
                if (files != null && files.length > 0) {
                    gamePath = files[0].getAbsolutePath();
                }
            }
        } else if (os.contains("windows")) {
            String steamFolderPath = System.getenv("ProgramFiles(x86)") + "\\Steam\\steamapps\\common\\AoCII\\AoC2.exe";
            File steamFolder = new File(steamFolderPath);
            if (steamFolder.exists()) {
                gamePath = steamFolderPath;
            }
        }
        return gamePath;
    }

    private void autoDetectGamePath() {
        String autoDetectedGamePath = findGamePath();
        if (autoDetectedGamePath != null) {
            gamePathTextField.setText(autoDetectedGamePath);
        } else {
            JOptionPane.showMessageDialog(null, "Automatic game path detection failed. Please provide the path manually.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private boolean isProgramInstalled(String toolName, String versionArgument) {
        try {
            Process process = new ProcessBuilder(toolName, versionArgument).start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String output = reader.readLine();
            process.waitFor();
            return output != null && !output.isEmpty();
        } catch (IOException | InterruptedException e) {
            return false;
        }
    }


    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new AoMInstaller();
            }
        });
    }
}
